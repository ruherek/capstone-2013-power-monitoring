import os, sys
sys.path.append('/home/pi/capstone-2013-power-monitoring')
os.environ['DJANGO_SETTINGS_MODULE'] = 'core.settings'
import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()

