# python script to generate fake data for the EnergyReading db
# script runs every few minutes to generate fake data associated with a device ID

import random
import urllib
from time import sleep

DEVICEID = "A01"
WEB_SERVER = "http://localhost/ReportUsage.php?"

LOWER_USAGE = 150
UPPER_USAGE = 500

# run this script forever
while True:
	powerUsage = random.uniform(LOWER_USAGE, UPPER_USAGE)

	url = WEB_SERVER + 'DeviceID=' + DEVICEID + '&PowerUsage=' + str(round(powerUsage, 2))
	print '%s' % url
	urllib.urlopen(url).read()

	sleep(180)
