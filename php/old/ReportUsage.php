<?php
	/*

		ReportPower.php

		Script is called by the arduino device to report power usage.

		CONFIGURATION:
		- MySQL Variables are below. Edit them as needed for DB, User, Pwd, Host, and Table

		USAGE:
		ReportPower.php?DeviceID=<value>&PowerUsageWatts=<value>;

	*/

	//Define all dates to be set at UTC
	date_default_timezone_set('UTC');

	//MySQL Settings
	$HOST = "localhost";
	$USER = "djangopowermon";
	$DB = "djangopowermon";
	$PASSWORD = "se2013";
	$TABLE = "power_monitor_energyreading";

	/* ============================================================ */

	$DeviceID = null;
	$PowerUsageWatts = null;

	//Get the Device ID and Power Usage via the URL / GET
	$DeviceID = htmlspecialchars($_GET["DeviceID"]);
	$PowerUsageWatts = htmlspecialchars($_GET["PowerUsage"]);

	//Check to make sure the values exist.. if not, display an error
	if($DeviceID == null || $PowerUsageWatts == null){
		echo 'ERROR: One or more of the required parameters does not exist. <br />';
		echo 'USAGE: ReportPower.php?DeviceID=&lt;value&gt;&PowerUsage=&lt;value&gt; <br />';
	}
	//We can go ahead and attempt to connect to insert the data
	else{
		$con = mysql_connect($HOST, $USER, $PASSWORD);

		//There was a connection error
		if(!con){
			die('Could not connect: ' . mysql_error());
		}

		//Get the time for the entry
		$time = date('Y-m-d H:i:s');

		mysql_select_db($DB, $con);
		mysql_query("INSERT INTO " . $TABLE . " (DeviceID, EnergyReadingWatts, ReadingTime) VALUES ('" . $DeviceID . "', '" . $PowerUsageWatts . "', '" . $time . "')");

		mysql_close($con);

		echo "OK";
	}
?>
