PHP Power Reading Script
==========

The script contained in this document is a PHP script. The script takes in a device ID, and a power reading (in watts). The arduino device calls this php script from a web page to insert new data into the appropriate tables. This script has NO AUTHENTICATION. It is not secure in that respect. Anyone with a URL, and knowledge of the correct parameters, can call the script and insert data into the power reading table.

## Requirements

To run this script, you will need a PHP enabled webserver, such as Apache2 with a PHP mod installed. You also need connectivity to a MySQL database. Please adjust the settings accordingly for your MySQL database.

## MIT License

Copyright (c) 2013 R. Uherek, A. Oza, M. Todiwala, A. Patel

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
