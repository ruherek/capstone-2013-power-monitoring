<?php
	/*

		ReportPower.php

		Script is called by the arduino device to report power usage.

		CONFIGURATION:
		- MySQL Variables are below. Edit them as needed for DB, User, Pwd, Host, and Table

		USAGE:
		ReportPower.php?DeviceID=<value>&PowerUsageWatts=<value>;

	*/

	//Define all dates to be set at UTC
	date_default_timezone_set('UTC');

	//MySQL Settings
	$HOST = "localhost";
	$USER = "djangopowermon";
	$DB = "djangopowermon";
	$PASSWORD = "se2013";
	$TABLE = "power_monitor_energyreading";

	/* ============================================================ */

	$DeviceID = null;
	$PowerUsageWatts = null;

	//Generate random data with prefix of A
	$prefix = 'A-';
	$number = sprintf("%02d", rand(1,10));
	$DeviceID = $prefix . $number; 
	$PowerUsageWatts = rand(1000,6000)/10;

	//Check to make sure the values exist.. if not, display an error
	if($DeviceID == null || $PowerUsageWatts == null){
		echo 'ERROR: One or more of the required parameters does not exist. <br />';
		echo 'USAGE: ReportPower.php?DeviceID=&lt;value&gt;&PowerUsage=&lt;value&gt; <br />';
	}
	//We can go ahead and attempt to connect to insert the data
	else{
		$con = mysql_connect($HOST, $USER, $PASSWORD);

		//There was a connection error
		if(!con){
			die('Could not connect: ' . mysql_error());
		}

		//Get the time for the entry
		$time = date('Y-m-d H:i:s', strtotime( '-'.mt_rand(0,30).' days'));

		mysql_select_db($DB, $con);
		$query = "INSERT INTO " . $TABLE . " (DeviceID, EnergyReadingWatts, ReadingTime) VALUES ('" . $DeviceID . "', '" . $PowerUsageWatts . "', '" . $time . "')";
		mysql_query($query);
		echo $query;

		mysql_close($con);

	}
?>
