EasyPower Power Monitor
==========

This is a Django-based site for the EasyPower 2013 Software Engineering Capstone. This site works with the Arduino device to monitor power for a building.

## Requirements

To run this software you need Python, and the Django based framework. You will also need a HTTP server (Apache2, preferably) and the WSGI mod enabled. A MySQL database is needed for the backend. If you wish to enable device tracking, you will also need php configured. The Arduino device will call a php script to update data in the database.

## MIT License

Copyright (c) 2013 R. Uherek, A. Oza, M. Todiwala, A. Patel

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
