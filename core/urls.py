from django.conf.urls import patterns, include, url
from django.contrib.sites.models import Site
from django.contrib.auth.models import Group

from power_monitor.models import EnergyUser

from django.contrib import admin
admin.autodiscover()

admin.site.unregister(Site)
admin.site.unregister(Group)

admin.site.register(EnergyUser)

urlpatterns = patterns('',
	url(r'^$', 'django.views.generic.simple.redirect_to', {'url': '/home/'}),
	url(r'^home/', include('power_monitor.urls')),
    url(r'^admin/', include(admin.site.urls)),
)
