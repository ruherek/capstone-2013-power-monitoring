-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 13, 2013 at 10:06 PM
-- Server version: 5.5.29
-- PHP Version: 5.4.6-1ubuntu1.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `djangopowermon`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE IF NOT EXISTS `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permissions`
--

CREATE TABLE IF NOT EXISTS `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_425ae3c4` (`group_id`),
  KEY `auth_group_permissions_1e014c8f` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission`
--

CREATE TABLE IF NOT EXISTS `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_1bb8f392` (`content_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add permission', 1, 'add_permission'),
(2, 'Can change permission', 1, 'change_permission'),
(3, 'Can delete permission', 1, 'delete_permission'),
(4, 'Can add group', 2, 'add_group'),
(5, 'Can change group', 2, 'change_group'),
(6, 'Can delete group', 2, 'delete_group'),
(7, 'Can add user', 3, 'add_user'),
(8, 'Can change user', 3, 'change_user'),
(9, 'Can delete user', 3, 'delete_user'),
(10, 'Can add content type', 4, 'add_contenttype'),
(11, 'Can change content type', 4, 'change_contenttype'),
(12, 'Can delete content type', 4, 'delete_contenttype'),
(13, 'Can add session', 5, 'add_session'),
(14, 'Can change session', 5, 'change_session'),
(15, 'Can delete session', 5, 'delete_session'),
(16, 'Can add site', 6, 'add_site'),
(17, 'Can change site', 6, 'change_site'),
(18, 'Can delete site', 6, 'delete_site'),
(19, 'Can add log entry', 7, 'add_logentry'),
(20, 'Can change log entry', 7, 'change_logentry'),
(21, 'Can delete log entry', 7, 'delete_logentry'),
(22, 'Can add energy user', 8, 'add_energyuser'),
(23, 'Can change energy user', 8, 'change_energyuser'),
(24, 'Can delete energy user', 8, 'delete_energyuser'),
(25, 'Can add energy reading', 9, 'add_energyreading'),
(26, 'Can change energy reading', 9, 'change_energyreading'),
(27, 'Can delete energy reading', 9, 'delete_energyreading');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user`
--

CREATE TABLE IF NOT EXISTS `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `password` varchar(128) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `last_login` datetime NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `auth_user`
--

INSERT INTO `auth_user` (`id`, `username`, `first_name`, `last_name`, `email`, `password`, `is_staff`, `is_active`, `is_superuser`, `last_login`, `date_joined`) VALUES
(1, 'root', '', '', 'rzermatt@gmail.com', 'pbkdf2_sha256$10000$iMrKwPY0ITIC$JJlEYyHTp6HJI29YZWmlQzSNk4Ju8dUlhsZ+jyzObUY=', 1, 1, 1, '2013-02-13 21:47:04', '2013-02-09 04:04:13'),
(11, 'rzermatt', 'Rylan', 'Uherek', 'rzermatt@gmail.com', 'pbkdf2_sha256$10000$f4WgouU8kZQu$t+sLC4Nbt92+HZrABEmsY+aTmNVvnGG4tLaohu3rZMc=', 0, 1, 0, '2013-02-14 03:09:53', '2013-02-12 16:59:16');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_groups`
--

CREATE TABLE IF NOT EXISTS `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_403f60f` (`user_id`),
  KEY `auth_user_groups_425ae3c4` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_user_permissions`
--

CREATE TABLE IF NOT EXISTS `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_403f60f` (`user_id`),
  KEY `auth_user_user_permissions_1e014c8f` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `django_admin_log`
--

CREATE TABLE IF NOT EXISTS `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_403f60f` (`user_id`),
  KEY `django_admin_log_1bb8f392` (`content_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `django_content_type`
--

CREATE TABLE IF NOT EXISTS `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_label` (`app_label`,`model`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `name`, `app_label`, `model`) VALUES
(1, 'permission', 'auth', 'permission'),
(2, 'group', 'auth', 'group'),
(3, 'user', 'auth', 'user'),
(4, 'content type', 'contenttypes', 'contenttype'),
(5, 'session', 'sessions', 'session'),
(6, 'site', 'sites', 'site'),
(7, 'log entry', 'admin', 'logentry'),
(8, 'energy user', 'power_monitor', 'energyuser'),
(9, 'energy reading', 'power_monitor', 'energyreading');

-- --------------------------------------------------------

--
-- Table structure for table `django_session`
--

CREATE TABLE IF NOT EXISTS `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_3da3d3d8` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('288cee0d8104c4c51df554389f31b90e', 'NDAxYjQxMzY5OTAwZWI4NWIwNzcxOTUyNGU4MjM2ODFiMWVhMTRjODqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZFUpZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmRxAlUN\nX2F1dGhfdXNlcl9pZIoBC3Uu\n', '2013-02-27 19:12:58'),
('62976575d23d27200c2910713e781612', 'MmIzMGVlYmEwMDM5NmIwZTYwMzQ0YWVmNzYxMWRiYzI3ODQ1NmM3ZDqAAn1xAS4=\n', '2013-02-28 06:03:56'),
('8a50fca3fd5c38b703a1fbc7ea8fd741', 'MmJjZThiNzc5MGI1M2MxYTk4ZDU2ZDVlMjhhZmMyMTEzMjVhMjZiMzqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQt1Lg==\n', '2013-02-27 21:16:35'),
('b8316265610795b1f73922df47772c9b', 'MmIzMGVlYmEwMDM5NmIwZTYwMzQ0YWVmNzYxMWRiYzI3ODQ1NmM3ZDqAAn1xAS4=\n', '2013-02-27 19:40:03'),
('e5d13c3d81df6f185dd38558eecda904', 'ZTYxY2Y4MDlkZTljZjc0NGJkODI3MzU3MGZhYWM3ZGY1YTY0YzEwYTqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQF1Lg==\n', '2013-02-23 04:06:06'),
('ec6c0043b0b21d7eac01ac0f96c8fdfa', 'MmJjZThiNzc5MGI1M2MxYTk4ZDU2ZDVlMjhhZmMyMTEzMjVhMjZiMzqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQt1Lg==\n', '2013-02-27 21:48:04');

-- --------------------------------------------------------

--
-- Table structure for table `django_site`
--

CREATE TABLE IF NOT EXISTS `django_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `django_site`
--

INSERT INTO `django_site` (`id`, `domain`, `name`) VALUES
(1, 'example.com', 'example.com');

-- --------------------------------------------------------

--
-- Table structure for table `power_monitor_energyreading`
--

CREATE TABLE IF NOT EXISTS `power_monitor_energyreading` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `DeviceID` varchar(255) NOT NULL,
  `EnergyReadingWatts` decimal(10,2) NOT NULL,
  `ReadingTime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=143 ;

--
-- Dumping data for table `power_monitor_energyreading`
--

INSERT INTO `power_monitor_energyreading` (`id`, `DeviceID`, `EnergyReadingWatts`, `ReadingTime`) VALUES
(3, 'A01', 491.39, '2013-02-13 13:37:14'),
(4, 'A01', 198.60, '2013-02-13 13:40:14'),
(5, 'A01', 278.18, '2013-02-13 13:43:14'),
(6, 'A01', 159.84, '2013-02-13 13:46:15'),
(7, 'A01', 313.26, '2013-02-13 13:49:15'),
(8, 'A01', 205.06, '2013-02-13 13:52:15'),
(9, 'A01', 338.14, '2013-02-13 13:55:15'),
(10, 'A01', 357.54, '2013-02-13 13:58:15'),
(11, 'A01', 303.15, '2013-02-13 14:01:15'),
(12, 'A01', 336.20, '2013-02-13 14:04:15'),
(13, 'A01', 418.92, '2013-02-13 14:07:15'),
(14, 'A01', 496.32, '2013-02-13 14:10:15'),
(15, 'A01', 379.89, '2013-02-13 14:13:15'),
(16, 'A01', 421.37, '2013-02-13 14:16:16'),
(17, 'A01', 410.95, '2013-02-13 14:19:16'),
(18, 'A01', 381.86, '2013-02-13 14:22:16'),
(19, 'A01', 456.73, '2013-02-13 14:25:16'),
(20, 'A01', 187.01, '2013-02-13 14:28:16'),
(21, 'A01', 438.51, '2013-02-13 14:31:16'),
(22, 'A01', 461.24, '2013-02-13 14:34:16'),
(23, 'A01', 320.10, '2013-02-13 14:37:16'),
(24, 'A01', 471.03, '2013-02-13 14:40:16'),
(25, 'A01', 472.39, '2013-02-13 14:43:16'),
(26, 'A01', 380.81, '2013-02-13 14:46:16'),
(27, 'A01', 427.59, '2013-02-13 14:49:17'),
(28, 'A01', 492.28, '2013-02-13 14:52:17'),
(29, 'A01', 302.50, '2013-02-13 14:55:17'),
(30, 'A01', 266.09, '2013-02-13 14:58:17'),
(31, 'A01', 290.88, '2013-02-13 15:01:17'),
(32, 'A01', 380.71, '2013-02-13 15:04:17'),
(33, 'A01', 263.35, '2013-02-13 15:07:17'),
(34, 'A01', 476.83, '2013-02-13 15:10:17'),
(35, 'A01', 218.41, '2013-02-13 15:13:17'),
(36, 'A01', 189.16, '2013-02-13 15:16:17'),
(37, 'A01', 475.33, '2013-02-13 15:19:18'),
(38, 'A01', 281.67, '2013-02-13 15:22:18'),
(39, 'A01', 223.06, '2013-02-13 15:25:18'),
(40, 'A01', 187.23, '2013-02-13 15:28:18'),
(41, 'A01', 424.22, '2013-02-13 15:31:18'),
(42, 'A01', 282.88, '2013-02-13 15:34:18'),
(43, 'A01', 428.94, '2013-02-13 15:37:18'),
(44, 'A01', 324.69, '2013-02-13 15:40:18'),
(45, 'A01', 478.97, '2013-02-13 15:43:18'),
(46, 'A01', 211.00, '2013-02-13 15:46:18'),
(47, 'A01', 293.19, '2013-02-13 15:49:19'),
(48, 'A01', 298.47, '2013-02-13 15:52:19'),
(49, 'A01', 348.63, '2013-02-13 15:55:19'),
(50, 'A01', 409.64, '2013-02-13 15:58:19'),
(51, 'A01', 494.26, '2013-02-13 16:01:19'),
(52, 'A01', 387.02, '2013-02-13 16:04:19'),
(53, 'A01', 335.58, '2013-02-13 16:07:19'),
(54, 'A01', 294.26, '2013-02-13 16:10:19'),
(55, 'A01', 248.09, '2013-02-13 16:13:19'),
(56, 'A01', 182.96, '2013-02-13 16:16:19'),
(57, 'A01', 309.93, '2013-02-13 16:19:19'),
(58, 'A01', 226.80, '2013-02-13 16:22:20'),
(59, 'A01', 180.71, '2013-02-13 16:25:20'),
(60, 'A01', 159.02, '2013-02-13 16:28:20'),
(61, 'A01', 495.12, '2013-02-13 16:31:20'),
(62, 'A01', 216.20, '2013-02-13 16:34:20'),
(63, 'A01', 449.68, '2013-02-13 16:37:20'),
(64, 'A01', 214.84, '2013-02-13 16:40:20'),
(65, 'A01', 282.94, '2013-02-13 16:43:20'),
(66, 'A01', 445.67, '2013-02-13 16:46:20'),
(67, 'A01', 208.08, '2013-02-13 16:49:20'),
(68, 'A01', 296.94, '2013-02-13 16:52:20'),
(69, 'A01', 440.48, '2013-02-13 16:55:21'),
(70, 'A01', 190.71, '2013-02-13 16:58:21'),
(71, 'A01', 472.59, '2013-02-13 17:01:21'),
(72, 'A01', 490.74, '2013-02-13 17:04:21'),
(73, 'A01', 204.18, '2013-02-13 17:07:21'),
(74, 'A01', 410.10, '2013-02-13 17:10:21'),
(75, 'A01', 298.35, '2013-02-13 17:13:21'),
(76, 'A01', 152.36, '2013-02-13 17:16:21'),
(77, 'A01', 422.89, '2013-02-13 17:19:21'),
(78, 'A01', 328.02, '2013-02-13 17:22:21'),
(79, 'A01', 352.57, '2013-02-13 17:25:21'),
(80, 'A01', 455.94, '2013-02-13 17:28:21'),
(81, 'A01', 331.56, '2013-02-13 17:31:22'),
(82, 'A01', 351.87, '2013-02-13 17:34:22'),
(83, 'A01', 382.53, '2013-02-13 17:37:22'),
(84, 'A01', 351.60, '2013-02-13 17:40:22'),
(85, 'A01', 422.98, '2013-02-13 17:43:22'),
(86, 'A01', 335.73, '2013-02-13 17:46:22'),
(87, 'A01', 150.38, '2013-02-13 17:49:22'),
(88, 'A01', 187.28, '2013-02-13 17:52:22'),
(89, 'A01', 449.64, '2013-02-13 17:55:22'),
(90, 'A01', 482.36, '2013-02-13 17:58:22'),
(91, 'A01', 441.01, '2013-02-13 18:01:23'),
(92, 'A01', 230.22, '2013-02-13 18:04:23'),
(93, 'A01', 328.14, '2013-02-13 18:07:23'),
(94, 'A01', 237.39, '2013-02-13 18:10:23'),
(95, 'A01', 203.49, '2013-02-13 18:13:23'),
(96, 'A01', 342.31, '2013-02-13 18:16:23'),
(97, 'A01', 320.58, '2013-02-13 18:19:23'),
(98, 'A01', 321.42, '2013-02-13 18:22:23'),
(99, 'A01', 323.61, '2013-02-13 18:25:23'),
(100, 'A01', 489.24, '2013-02-13 18:28:23'),
(101, 'A01', 246.11, '2013-02-13 18:31:23'),
(102, 'A01', 477.48, '2013-02-13 18:34:23'),
(103, 'A01', 375.87, '2013-02-13 18:37:24'),
(104, 'A01', 386.08, '2013-02-13 18:40:24'),
(105, 'A01', 246.09, '2013-02-13 18:43:24'),
(106, 'A01', 353.04, '2013-02-13 18:46:24'),
(107, 'A01', 157.60, '2013-02-13 18:49:24'),
(108, 'A01', 460.53, '2013-02-13 18:52:24'),
(109, 'A01', 329.66, '2013-02-13 18:55:24'),
(110, 'A01', 369.51, '2013-02-13 18:58:24'),
(111, 'A01', 226.50, '2013-02-13 19:01:24'),
(112, 'A01', 468.83, '2013-02-13 19:04:24'),
(113, 'A01', 404.47, '2013-02-13 19:07:24'),
(114, 'A01', 297.34, '2013-02-13 19:10:25'),
(115, 'A01', 181.61, '2013-02-13 19:13:25'),
(116, 'A01', 308.56, '2013-02-13 19:16:25'),
(117, 'A01', 279.85, '2013-02-13 19:19:25'),
(118, 'A01', 220.12, '2013-02-13 19:22:25'),
(119, 'A01', 336.38, '2013-02-13 19:25:25'),
(120, 'A01', 285.88, '2013-02-13 19:28:25'),
(121, 'A01', 374.86, '2013-02-13 19:31:25'),
(122, 'A01', 342.30, '2013-02-13 19:34:25'),
(123, 'A01', 229.29, '2013-02-13 19:37:25'),
(124, 'A01', 164.56, '2013-02-13 19:40:25'),
(125, 'A01', 212.47, '2013-02-13 19:43:25'),
(126, 'A01', 385.20, '2013-02-13 19:46:26'),
(127, 'A01', 436.52, '2013-02-13 19:49:26'),
(128, 'A01', 239.44, '2013-02-13 19:52:26'),
(129, 'A01', 165.69, '2013-02-13 19:55:26'),
(130, 'A01', 209.81, '2013-02-13 19:58:26'),
(131, 'A01', 379.13, '2013-02-13 20:01:26'),
(132, 'A01', 244.62, '2013-02-13 20:04:26'),
(133, 'A01', 338.71, '2013-02-13 20:07:26'),
(134, 'A01', 152.50, '2013-02-13 20:10:26'),
(135, 'A01', 411.41, '2013-02-13 20:13:26'),
(136, 'A01', 216.60, '2013-02-13 20:16:27'),
(137, 'A01', 439.28, '2013-02-13 20:19:27'),
(138, 'A01', 258.62, '2013-02-13 20:22:27'),
(139, 'A01', 168.81, '2013-02-13 20:25:27'),
(140, 'A01', 189.45, '2013-02-13 20:28:27'),
(141, 'A01', 209.84, '2013-02-13 20:31:27'),
(142, 'A01', 487.76, '2013-02-13 20:34:27');

-- --------------------------------------------------------

--
-- Table structure for table `power_monitor_energyuser`
--

CREATE TABLE IF NOT EXISTS `power_monitor_energyuser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `DeviceID` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `power_monitor_energyuser`
--

INSERT INTO `power_monitor_energyuser` (`id`, `user_id`, `DeviceID`) VALUES
(3, 11, 'A01');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `group_id_refs_id_3cea63fe` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `permission_id_refs_id_5886d21f` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`);

--
-- Constraints for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `content_type_id_refs_id_728de91f` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Constraints for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `user_id_refs_id_7ceef80f` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  ADD CONSTRAINT `group_id_refs_id_f116770` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Constraints for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `user_id_refs_id_dfbab7d` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  ADD CONSTRAINT `permission_id_refs_id_67e79cb` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`);

--
-- Constraints for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `user_id_refs_id_c8665aa` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  ADD CONSTRAINT `content_type_id_refs_id_288599e6` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Constraints for table `power_monitor_energyuser`
--
ALTER TABLE `power_monitor_energyuser`
  ADD CONSTRAINT `user_id_refs_id_3856de2a` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
