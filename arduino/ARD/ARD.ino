#include <SPI.h>
#include <Ethernet.h>

#define S1_PIN 0    // input pin for first inductive sensor
#define S2_PIN 1    // input pin for second inductive sensor
#define SAMPLES 10000 // number of samples to take
#define NUM_READINGS 10 // number of readings to average



float val1 = 0;
float val2 = 0;
int reading_cnt = 0; // counter for 1 second samples
float total_rms1 = 0;
float total_rms2 = 0;


/* SET THESE IPS TO THE PROPER ONES FOR WHERE YOU ARE !!!! */
byte mac[] = { 0x90, 0xA2, 0xDA, 0x0D, 0xAA, 0x3B };
byte ip[] = { 172, 16, 2, 37 };
byte gateway[] = { 172, 16, 1, 1 };
byte subnet[] = { 255, 255, 0, 0 };
byte server[] = { 172, 16, 2, 29 }; // VM


EthernetClient client;

void setup() {
  Ethernet.begin(mac, ip);
  analogReference(EXTERNAL);
  Serial.begin(9600);

  delay(1000);
  Serial.println("Connecting...");

}

void loop() {

  int max_val1 = 0;
  int max_val2 = 0;
  float rms_val1 = 0;
  float rms_val2 = 0;

  for (int cnt=0; cnt < SAMPLES; cnt++) {     
  val1 = analogRead(S1_PIN);   
  val2 = analogRead(S2_PIN); 
  

  rms_val1 = rms_val1 + sq((float)val1);  
  rms_val2 = rms_val2 + sq((float)val2);  
  }

  
  rms_val1 = sqrt(rms_val1 / (SAMPLES/2) ); 
  rms_val2 = sqrt(rms_val2 / (SAMPLES/2) );   
  total_rms1 = total_rms1 + rms_val1;  
  total_rms2 = total_rms2 + rms_val2;   
  reading_cnt++; 


  if (reading_cnt >= NUM_READINGS) {

    float average_rms1 = (total_rms1 / NUM_READINGS);
    float average_rms2 = total_rms2 / NUM_READINGS;
    
    Serial.print("AVG VALUES *RMS* - ");
    Serial.print(46.37*average_rms1);
    Serial.print("   ");
    Serial.println(val1);
        

    if (client.connect(server,80)) {
      Serial.println("Sending data");

     client.print("GET /ReportUsage.php?DeviceID=DEMO&PowerUsage=");
      client.print(46.37*average_rms1);
      client.println(" HTTP/1.0");
      client.println();
      client.stop();
    } else {
      Serial.println("Failed to connect to client");
      } 
    total_rms1 = 0;
    total_rms2 = 0;
    reading_cnt = 0;
  } 

}
