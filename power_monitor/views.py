from django.template import Context, loader
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib.auth import logout as auth_logout
from django.contrib.auth import login as auth_login
from django import forms
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from registration import RegistrationForm
from models import EnergyUser, EnergyReading
from datetime import datetime,date,timedelta
from django.utils.timezone import utc
from calendar import monthrange
import calendar

# get the home page
def home(request):
	context = RequestContext(request)
	return render_to_response('home.html', context)

# about view
def project(request):
	context = RequestContext(request)
	return render_to_response('project.html', context)

# contact view
def contact(request):
	context = RequestContext(request)
	return render_to_response('contact.html', context)

# login view
def login(request):
	context = RequestContext(request)
	if request.method == "POST":
		username = request.POST.get('username')
		password = request.POST.get('pass')

		# attempt to authenticate the username and password
		user = authenticate(username=username, password=password)
		error = ""

		if not (username and password):
			error = "ERROR: One or more of the fields is blank."
		elif user is None:
			error = "ERROR: Sorry, the login you provided was invalid."
		elif not user.is_active:
			error = "ERROR: Sorry, your account is inactive."
		else:
			auth_login(request, user)

		# handle an error
		if error:
			c = RequestContext(request, {'state':error})
			return render_to_response('home.html', c)
		else:
			# reverse to the dashboard
			return HttpResponseRedirect(reverse('dashboard'))

	return render_to_response('home.html', context)

def logout(request):
	auth_logout(request)
	return HttpResponseRedirect(reverse('home'))

def dashboard(request):
	context = RequestContext(request)
	timenow = datetime.utcnow().replace(tzinfo=utc)
	KWHOUR_COST = 0.1

	if not request.user.is_authenticated():
		return render_to_response('home.html', context)
	else:
		
		if request.user.has_perm('energyuser.change_energyuser'):
		#User is a staff member

			###Data for aggregate number of users
			readings = EnergyReading.objects.filter(ReadingTime__year=timenow.year, ReadingTime__month=timenow.month)
			total_users = EnergyUser.objects.all()
			total_usage = 0
			total_cost = 0 # Take the total cost
			for reading in readings:
				total_usage += reading.EnergyReading

			
			## Data for the last 30 days 
			last30 = [] #Will hold a 30 place array for last 30 days
			start = 0
			todays_date = date.today()
			while start < 30: #Loop through last 30 days
				todays_date = todays_date - timedelta(days=1)
				day_readings = EnergyReading.objects.filter(ReadingTime__year=todays_date.year, ReadingTime__month=todays_date.month, ReadingTime__day=todays_date.day ) #Get readings for specific day
				last30.append(0) #Add new spot into array for this data

				total = 0 #Store in temp variable
				for reading in day_readings:
					total += reading.EnergyReading
				avg_reading = total/day_readings.count()
				entry = {'date' : todays_date, 'total_power' : avg_reading, 'total_revenue' : float(avg_reading) * KWHOUR_COST/1000  } # Create dictionary so we have the data we need
				last30[start] = entry
				start+=1
				total_cost += round(float(avg_reading) * KWHOUR_COST/1000,2)
			
			return render_to_response('pcompany_dashboard.html', {'revenue30': total_cost ,'total_users' :total_users, 'total_usage' : total_usage, 'last30' : last30 }, context)

		else:
			# render the dashboard

			# find the device ID
			user_id = request.user.id
			dev_id = EnergyUser.objects.filter(user=user_id).values('DeviceID')

			# get the energy readings which match the device ( last 10 readings )
			readings = EnergyReading.objects.filter(DeviceID=dev_id).order_by('-ReadingTime')[:10]
	
			# find out if the device has been reporting within the last 30 minutes
			currentlyUpdating = False
			
	
			if len(readings) > 0:
				lastreading = readings[0].ReadingTime
				delta = (timenow-lastreading).total_seconds()
	
				# define the time interval for "active" data... 30 minutes
				INTERVAL = 30 * 60
	
				# assume that data is not being updated.. if it is, flip the bool
				if delta < INTERVAL:
					currentlyUpdating = True
	
			else:
				currentlyUpdating = False
	
			# get the readings from the past 24 hours
			last24 = EnergyReading.objects.filter(DeviceID=dev_id).filter(ReadingTime__year=timenow.year, ReadingTime__month=timenow.month, ReadingTime__day=timenow.day).order_by('-ReadingTime')
			
	
			averagepower = 0
			monthreadings = EnergyReading.objects.filter(DeviceID=dev_id).filter(ReadingTime__year=timenow.year, ReadingTime__month=timenow.month)
	
			for reading in monthreadings:
				averagepower += reading.EnergyReading
	
			# average the monthly readings
			if not len(monthreadings) == 0:
				averagepower = averagepower/len(monthreadings)
	
			# find the number of seconds since the beginning of the month and the number of days in the month (this gives us how far we are into the month)
			# our power bill is estimated by finding out the number of days in the month, our average, and how far into the month we are. (e.g 60% into month @ $100 bill = $60 so far)
			hoursSoFar = timenow.day * 24
			actualBill = round(float((averagepower/1000)) * hoursSoFar * KWHOUR_COST , 2)
	
			return render_to_response('dashboard.html', {'readings':readings, 'realdata': currentlyUpdating, 'last24' : last24, 'actualBill' : actualBill}, context)

# register view
def register(request):
	context = RequestContext(request)
	newForm = RegistrationForm()

	if request.POST:
		form = RegistrationForm(request.POST)
		if form.is_valid():
			new_user = form.create_user()
			# link the user to the device
			form.create_EnergyUser(new_user)
			
			return render_to_response('home.html', context)
		else:
			# return the old form and any errors
			newForm = form
	
	return render_to_response('register.html', {'form':newForm} , context)

# details view
def details(request):
	context = RequestContext(request)

	if not request.user.is_authenticated():
		return render_to_response('home.html', context)
	else:
		# find the device ID
		user_id = request.user.id
		dev_id = EnergyUser.objects.filter(user=user_id).values('DeviceID')

		# send in the all time usage history
		allhistory = EnergyReading.objects.filter(DeviceID=dev_id)

		# get the usage data for the last year
		today = datetime.utcnow().replace(tzinfo=utc)

		powerhistory = []

		# loop over the last 12 months
		for x in range (0, 12):
			monthsago = x

			year = today.year
			month = today.month - x

			# if we are in last year, find out which month we are in
			if month <= 0:
				year = today.year - 1
				monthsago = 12 + month
				month = monthsago

			# find the estimated power bill and slap it into a dictionary
			fullname = str(calendar.month_name[month]) + " " + str(year)

			# estimate the power bill for the month
			KWHOUR_COST = 0.1

			averagepower = 0
			monthreadings = EnergyReading.objects.filter(DeviceID=dev_id).filter(ReadingTime__year=year, ReadingTime__month=month)

			for reading in monthreadings:
				averagepower += reading.EnergyReading

			# average the monthly readings
			if not len(monthreadings) == 0:
				averagepower = averagepower/len(monthreadings)

			# find the number of seconds since the beginning of the month and the number of days in the month (this gives us how far we are into the month)
			# our power bill is estimated by finding out the number of days in the month, our average, and how far into the month we are. (e.g 60% into month @ $100 bill = $60 so far)
			actualBill = 0

			if not (month == today.month and year == today.year):
				# we are dealing with an older month
				daysingivenmonth = monthrange(year, month)[1]
				actualBill = round(float((averagepower/1000)) * daysingivenmonth * 24 * KWHOUR_COST , 2)
			else:
				# we are in this month
				hoursSoFar = today.day * 24
				actualBill = round(float((averagepower/1000)) * hoursSoFar * KWHOUR_COST , 2)

			powerhistory.append({'month':fullname, 'reading':actualBill})

		return render_to_response('details.html', {'allhistory':allhistory, 'powerhistory':powerhistory}, context)
