from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = patterns('',
	url(r'^$', 'power_monitor.views.home', name='home'),
	url(r'^login/', 'power_monitor.views.login', name='login'),
	url(r'^logout/', 'power_monitor.views.logout', name='logout'),
	url(r'^project/', 'power_monitor.views.project', name='project'),
	url(r'^contact/', 'power_monitor.views.contact', name='contact'),
	url(r'^register/', 'power_monitor.views.register', name='register'),
	url(r'^dashboard/', 'power_monitor.views.dashboard', name='dashboard'),
	url(r'^details/', 'power_monitor.views.details', name='details'),
)

urlpatterns += staticfiles_urlpatterns()
