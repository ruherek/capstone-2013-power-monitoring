from django.db import models
from django.contrib.auth.models import User

class EnergyUser(models.Model):
	user = models.OneToOneField(User, blank=True, related_name='energy_user')
	DeviceID = models.CharField(max_length=255, db_column='DeviceID')

	def __unicode__(self):
		return self.user.username

	def get_devid(self):
		return self.DeviceID

class EnergyReading(models.Model):
	DeviceID = models.CharField(max_length=255, db_column='DeviceID')
	EnergyReading = models.DecimalField(max_digits=10, decimal_places=2, db_column='EnergyReadingWatts')
	ReadingTime = models.DateTimeField('date read')

	def __unicode__(self):
		return self.DeviceID + " " + str(self.ReadingTime)
